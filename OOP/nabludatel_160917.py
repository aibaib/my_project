from random import random

class Display: #создаём интерфейс функции, которая выводит то что нам нужно на экран
    def display(self):
        raise NotImplementedError()




class Subgect: #создаём интерфейс того что можно делать с наблюдателем, а именно:
    def pull_observer(self): #отписывать наблюдателя из списка рассылок
        raise NotImplementedError()

    def notify_observer(self):# оповещать наблюдателя
        raise NotImplementedError()

    def push_observer(self):#добавлять наблюдателя в список рассылок
        raise NotImplementedError()




class pribor: #создаём интерфейс прибор, с помощью которого мы можем считывать показания температуры, влажности, давления

    def Temperatura(self):
        raise NotImplementedError

    def pressure(self):
        raise NotImplementedError

    def humidity(self):
        raise NotImplementedError




class observer: #создаём интерфей, который будет отсылать пользователям данные с прибора
    def __init__(self):
        self.temperatura=None
        self.humidity=None
        self.pressure=None

    def restart(self,temperatura,himidity,pressure):
        self.temperatura=temperatura
        self.humidity=himidity
        self.pressure=pressure



class statistic():
    def statistic(self):
        raise NotImplementedError


class Prognoz:
    def prognoz(self):
        raise NotImplementedError

#СОЗДАЛИ ВСЕ НЕОБХОДИМЫЕ НАМ ИНТЕРФЕЙСЫ
#НАЧИНАЕ ПИСАТЬ РЕАЛИЗАЦИИ




class current_situation(observer,Display):
    def __init__(self): #инициализируем переменную s (какие-то показания датчиков)
        self.s=None
        self.t=None
        self.p=None

    def restart(self,s,t,p): #пишем реализацию функции restart, которая принимает значение s
        self.s=s
        self.t=t
        self.p=p

    def display(self): #реализация функции display, которая выводит значение s(какое-то показание)
        print('Temoeratura: {}'.format(self.t))
        print('humidity: {}'.format(self.s))
        print('pressure: {}'.format(self.p))





class whether(Subgect,pribor):
    def __init__(self): #инициализируем переменные
        self.observers=[]
        self.s=None
        self.t=None
        self.p=None

    def pull_observer(self,ob): #пишем реализацию интерфейсу отписывания наблюдателя из рассылки
        self.observers.remove(ob)

    def notify_observer(self): #пишем реализацию интерфейса для оповещения подписчиков
        for element in self.observers:# для какждого элемента в массиве наблюдателей
            element.restart(self.s,self.t,self.p)# вызываем функцию передачи показания пользователю

    def push_observer(self,ob): #реализация добавления наблюдателя в список
        if isinstance(ob,observer): #проверяем его тип данных
            if ob in self.observers: # проверяем если элемент уже есть в списке наблюдателей
                raise TypeError('this observer already exit') #то выводим ошибку
            else:
                self.observers.append(ob) #иначе добовляем элемент в список
        else:
            raise TypeError('{0} not is observer'.format(type(ob))) #если объект не подходит нужному типу данных, то выводим ошибку

    def Temperatura(self): # функция (реализация), которрая считывает показания температуры
        return random()

    def pressure(self): #показания давления
        return 10*random()

    def humidity(self):# показания влажности
        return 100*random()

    def changes(self): #пишем функцию
        self.t=self.Temperatura()
        self.s=self.humidity()
        self.p=self.pressure()
        self.notify_observer()






class Statistic_display(statistic, observer, Display):
    def __init__(self):
        self.massiv_temperatura = []
        self.massiv_humidity = []
        self.massiv_pressure = []

        self.t=None
        self.s=None
        self.p=None

        self.average_temperatura=None
        self.average_humidity=None
        self.average_pressure=None


    def restart(self,s,t,p):
        self.t=t
        self.s=s
        self.p=p


        self.massiv_temperatura.append(self.t)
        self.massiv_humidity.append(self.s)
        self.massiv_pressure.append(self.p)
        self.statistic()

    def statistic(self):

        if len(self.massiv_temperatura)!=0:
            self.average_temperatura=sum(self.massiv_temperatura)/len(self.massiv_temperatura)
            self.average_humidity=sum(self.massiv_humidity)/len(self.massiv_humidity)
            self.average_pressure=sum(self.massiv_pressure)/len(self.massiv_humidity)

        else:
            raise ZeroDivisionError()

    def display(self):
        print('Average temperatura: {}'.format(self.average_temperatura))
        print('Average himidity: {}'.format(self.average_humidity))
        print('Average pressure: {}'.format(self.average_pressure))


if __name__=='__main__':

    w=whether()
    c=current_situation()

    q=Statistic_display()



    w.push_observer(c)
    w.push_observer(q)
    w.changes()
    c.display()
    q.display()

