# пишем паттерн без перехватчиков
class CaffeineBeverage:
    def prepareRecipe(self):
        self.boilWater()
        self.brew()
        self.pourlnCup()
        self.addCondiments()

    def brew(self):
        raise NotImplementedError
    def addCondiments(self):
        raise NotImplementedError

    def boilWater(self):
        print('Boiling water')

    def pourlnCup(self):
        print('Pouring into cup')



class Coffee(CaffeineBeverage):

    def brew(self):
        print('Dripping coffee through filter')

    def addCondiments(self):
        print('Adding Sugar and Milk')



class Tea(CaffeineBeverage):
    def brew(self):
        print('Steeping the tea')

    def addCondiments(self):
        print('Adding Lemon')




#ну и там можно написать вывод и всё работает
#if __name__=='__main__':
#   myTea=Tea()
#   myTea.prepareRecipe()







#теперь пишем тот же паттерн, но уже с перехватчиками



class CaffeineBeverageWithHook:
    def prepareRecipe(self):
        self.boilWater()
        self.brew()
        self.pourlnCup()
        self.p=self.customerWantsCondiments()
        if self.p==True:
            self.addCondiments()

    def brew(self):
        raise NotImplementedError
    def addCondiments(self):
        raise NotImplementedError

    def boilWater(self):
        print('Boiling water')

    def pourlnCup(self):
        print('Pouring into cup')

    def customerWantsCondiments(self):
        raise NotImplementedError

class CoffeWithHook(CaffeineBeverageWithHook):


    def brew(self):
        print('Dripping coffee through filter')

    def addCondiments(self):
        print('Adding Sugar and Milk')


    def customerWantsCondiments(self):
        self.answer=self.getUserInput()
        if self.answer=='yes':
            return True
        else:
            return False

    def getUserInput(self):
        self.answer=None
        print('Would you like milk and sugar with your coffee?')
        self.answer=input()
        if self.answer==None:
            return 'no'
        else:
            return self.answer


class TeaWithHook(CaffeineBeverageWithHook):
    def brew(self):
        print('Steeping the tea')

    def addCondiments(self):
        print('Adding Lemon')


    def customerWantsCondiments(self):
        self.answer=self.getUserInput()
        if self.answer=='yes':
            return True
        else:
            return False

    def getUserInput(self):
        self.answer=None
        print('Would you like lemon with your tea?')
        self.answer=input()
        if self.answer==None:
            return 'no'
        else:
            return self.answer


if __name__=='__main__':
    teaHook=TeaWithHook()
    CoffeeWithHook=CoffeWithHook()

    print('making tea...')
    teaHook.prepareRecipe()
    print()
    print()
    print('making coffee...')
    CoffeeWithHook.prepareRecipe()


