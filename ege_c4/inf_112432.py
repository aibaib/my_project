n=int(input())
s=dict()
for i in range(n):
    a=input()
    if a in s:
        s[a]+=1
    else:
        s[a]=1


for i in sorted(s.items(), key=lambda item: (-item[1],item[0])):
    print(i[0])