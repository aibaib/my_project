a=input()
index=a.find('.')
a=a[:index]
s=[0]*26
for i in range(len(a)):
    nomer=ord(a[i])-65
    s[nomer]+=1

flag=0
for i in range(26):
    if s[i]!=0:
        if s[i]%2!=0:
            flag+=1

if flag>1:
    print('NO')
elif flag==0:
    print('YES')
    otvet=[]
    for i in range(26):
        if s[i]!=0:
            if s[i]%2==0:
                for j in range(s[i]//2):
                    otvet.append(chr(i+65))
    print(''.join(map(str,otvet)),end='')
    otvet.reverse()
    print(''.join(map(str,otvet)))
else:
    otvet=[]
    print('YES')
    for i in range(26):
        if s[i]!=0:
            if s[i]%2==0:
                for j in range(s[i]//2):
                    otvet.append(chr(i+65))
            else:
                dobavka=chr(i+65)
                for l in range(s[i]//2):
                    otvet.append(chr(i+65))
    print(''.join(map(str,otvet)),end='')
    otvet.reverse()
    print(dobavka,end='')
    print(''.join(map(str,otvet)))
