a,b=map(int,input().split())
s=[0]*a
for i in range(b):
    c,d=map(int,input().split())
    s[c-1]+=1
    s[d-1]+=1
print(' '.join(map(str,s)))