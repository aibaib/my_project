import requests as r
from urllib.parse import urljoin
import os.path


# по текущему адресу страницы и url - пути изображения на странице возвращает полный url изображения f0  urllib.parse.urljoin(url1, url2)
# по адресу сайта она возвращает все url изображений f1

# по url возвращает название изображения f2
# по пути и имени изображения возвращает полный путь до изображения f3   # os.join(path1, path2)
# save_img_by_url(url, path)

"""
site  =
path =
names_img = []
url_img_list = f1(site)
for url in url_img_list:
    names_img.append(f2(url))

for i, url_img in enumerate(url_img_list):
    save_img_by_url(url_img, f3(path, names_img[i])

"""

def f0(url1,url2): #по текущему адресу страницы и url - пути изображения на странице возвращает полный url изображения
     return urljoin(url1, url2)

def f1(adress):
    s=[]
    k = r.get(adress).text.split('img ')  # получаю полный адресс строки и разбиваю его по img
    for i in range(len(k)):  # иду циклом по разбитой строке
        n = k[i].find('src') + 5  # ищу начало ссылки на изображение
        e = k[i].find('"', n)  # ищу конец ссылки на изобрадение
        url2 = k[i][n:e]  # беру слайс получаю  ссылку на изображение
        s.append(url2)
    return s #получаю массив url картинок на сайте

def f2(s): # по адресу сайта она возвращает все url изображений
    t=[]
    i=1
    for j in range(len(s)):
        l=s[j].rfind('/') #нахожу с конца строки первый слэш
        name=s[j][l:len(s[j])] #получил название файла
        if name in t: #если имя элемента есть в списке, то добавляем индекс
            h=name+'_{}'.format(i)
            t.append(h)
            i+=1
        else:
            t.append(name)
    return t #получаю массив названий

def f3(path1,path2): # по пути и имени изображения возвращает полный путь до изображения
    return os.path.join(path1, path2)

def save_img_by_url(m, path): #функция, сохраняющая изображения по заданному пути
    p = r.get(m, stream=True)
    if p.status_code == 200:
        with open(path, 'wb') as f:
            for chunk in p:
                f.write(chunk)

site = 'http://lyceum.urfu.ru/main/'
path = 'Y:'
#names_img = []
url_img_list = f1(site) #получин массив ссылок на изображения
print(url_img_list)
"""
#for url in url_img_list:
#    names_img.append(f2(url))
names_img=f2(url_img_list) #получили название каждого изображения
for i, url_img in enumerate(url_img_list):
    save_img_by_url(url_img, f3(path, names_img[i])) #сохраняем каждое изображение
"""


