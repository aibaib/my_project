import matplotlib.pyplot as plt

#пусть n это кол-во заболевших в начале, т.е равно 1

z=0#число заболевших
w=0#кол-во пробалевших
v=0#число выздоровивших в данный день
s=[]#массив числа заболевших
q=[]#массив времени (просто номера дней)
u=[]#массив кол-во больных


#функция Эйлера для этой задачи
def Euiler(L,k,n,w,v): #получает на вход всего человек, коэффициент k, и значения уже проболевших и больных в данный момент
    i=0 #введём два счётчика
    r=1
    h=0#введём переменную для ответа на вопрос, когда закончится эпидемия
    maxx=0 #введём новую переменную для ответа на вопрос какого максимальное кол-во человек больных в один день

    for j in range(100): #цикл с большим кол-во итераций, по скольким точкам мы будем строить график
        q.append(r)#создаём массив времени

        if r>7: #определяем, что после 7 дня люди начинают выздораливать
            v=s[i]
            i+=1
        w+=v # число переболевших

        z=k*n*((L-n-w)/L) #число заболевших
        s.append(z) #создаём массив заболевших в i день

        n=n+z-v #число больных
        u.append(n) #массив кол-ва людей которые больны в данный день

        if n>maxx:#просто находим максимум
            maxx=n

        r+=1

        if round(n)<=1 and r>9:#когда число больных станет меньше 4, то эпидемия закончилась, еденица, а не ноль из-за того что мы ничего не округляем
            h=r#присваеваем этот день переменной h
            break

   # w-ответ на вопрос сколько человек проболеет
    # L-w ответ на вопрос сколько человек вообще не заболеет

    return h,w,L-w,maxx




#вводим данные в задаче нам велечины
L=1000
k=0.5
n=1

h,w,m,maxx=Euiler(L,k,n,w,v)#вызываем функцию Эйлера, которая заполняет массив времени и массив числа больных в i День
#также она возвращает ответы на вопросы, поставленные в задаче


print(u)#печатаем значения числа больных в какой то день для наглядности
print(q)


#печатаем ответы на вопросы из задачи

print(h)#день когда кончится эпидемия
print(round(w))#число проболевших во время эпидемии
print(round(m))#число не заболевших вообще
print(round(maxx))#максимальное число больных в день

#строим график
graph = plt.figure()
ax = graph.add_subplot(111)
ax.plot(q,u,'r')
plt.show()