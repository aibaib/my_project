n,m=map(int,input().split())
for i in range(0,n):
    for j in range(0,m):
        if (j%2==0 and i%2==0) or (j%2!=0 and i%2!=0):
            print('.',end=' ')
        else:
            print('*',end=' ')
    print()