n,m=map(int,input().split())
s=[]
for x in range(0,n):
    s.append([0]*m)
#print(s)
for i in range(n):
    for j in range(m):
        if i==0 or j==0:
            s[i][j]=1
        else:
            s[i][j]=s[i-1][j]+s[i][j-1]
    #print()
#print(s)
for i in range(n):
    for j in range(m):
        if s[i][j]//10==0:
            print('    ',s[i][j],end='')
        elif s[i][j]//100==0:
            print('   ',s[i][j],end='')
        elif s[i][j]//1000==0:
            print('  ',s[i][j],end='')
        elif s[i][j]//10000==0:
            print(' ',s[i][j],end='')
        elif s[i][j]//1000000==0:
            print('',s[i][j],end='')
    print()
