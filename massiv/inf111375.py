import math
a=int(input())
s=[]
for x in range(a):
    s.append([0]*20)
for i in range(a):
    for j in range(20):
        if i==j or j==0:
            s[i][j]=1
        elif i-j<0:
            s[i][j]=' '
        elif j-i<0:
            s[i][j]=math.factorial(i)//(math.factorial(j)*math.factorial(i-j))
#print(s)
for i in range(a):
    for j in range(20):
        if s[i][j]!=' ':
            if s[i][j]//10==0:
                print('    ',s[i][j],end='')
            elif s[i][j]//100==0:
                print('   ',s[i][j],end='')
            elif s[i][j]//1000==0:
                print('  ',s[i][j],end='')
            elif s[i][j]//10000==0:
                print(' ',s[i][j],end='')
            elif s[i][j]//1000000==0:
                print('',s[i][j],end='')
    print()