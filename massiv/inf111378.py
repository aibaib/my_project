n,m=map(int,input().split())
s=[]
k=1
l=0
for x in range(n):
    s.append([0]*m)
for i in range(n):
    for j in range(m):
        if i%2==0:
            s[i][j]=k
            l=k
            k+=1
        else:
            s[i][j]=l+m
            l-=1
            k+=1
for i in range(n):
    for j in range(m):
        if s[i][j]!=' ':
            if s[i][j]//10==0:
                print('  ',s[i][j],end='')
            elif s[i][j]//100==0:
                print(' ',s[i][j],end='')
            elif s[i][j]//1000==0:
                print('',s[i][j],end='')
    print()
