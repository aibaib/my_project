n,m=map(int,input().split())
s=[]
for x in range(n):
    s.append([0]*m)
k=1
for i in range(n):
    for j in range(m):
        if i%2!=0:
            if j%2==0:
                s[i][j]='0'
            else:
                s[i][j]=k
                k+=1
        else:
            if j%2==0:
                s[i][j]=k
                k+=1
            else:
                s[i][j]='0'
for i in range(n):
    for j in range(m):
        if s[i][j]!='0':
            if s[i][j]//10==0:
                print('  ',s[i][j],end='')
            elif s[i][j]//100==0:
                print(' ',s[i][j],end='')
            elif s[i][j]//1000==0:
                print('',s[i][j],end='')
        else:
            print('  ','0',end='')
    print()
