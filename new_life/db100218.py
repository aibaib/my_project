
import sqlite3
conn = sqlite3.connect('products.db')
c = conn.cursor()

# подготовка запроса на выполнение
drop_table_Product = '''
DROP TABLE IF EXISTS Product;
'''

drop_table_Customer='''
DROP TABLE IF EXISTS Customer;
'''

drop_table_Purchase='''
DROP TABLE IF EXISTS Purchase;
'''

create_table_Product = '''
CREATE TABLE Product(
id INTEGER,
name TEXT,
price FLOAT
);
'''

create_table_Customer= '''
CREATE TABLE Customer(
id INTEGER,
fio TEXT,
adress TEXT
);
'''

create_table_Purchase='''
CREATE TABLE Purchase(
id INTEGER,
datetime TEXT,
id_customers INTEGER,
id_product INTEGER,
count INTEGER
);
'''



values_Product = [
    [1, 'iphone X','80000'],[2, 'ipad pro','40000'],
    [3, 'iphone 9','30000'],[4, 'macbook pro','120000'],
    [5, 'ice-cream', '10']
]

values_Customer=[
    [1,'Иванов Иван Иванович','г.Екатеринбург ул. Бажова д. 5 кв 7'],
    [2,'Александров Александр Александрович','г.Асбест ул. Парижская 89 кв 98'],
    [3,'Петров Петр Петрович','г.Екатеринбург ул. Ленина д. 1 кв 1']

]

values_Purchase=[
    [1,'2018-01-01 22:12:01',1,1,10],
    [2,'2018-01-01 22:59:59',2,2,1],
    [3,'2018-01-02 07:00:01',3,2,2],
    [4,'2018-02-01 12:30:43',3,5,1],
    [5,'2017-12-30 16:23:04',1,4,5],
    [6,'2017-02-03 19:04:39',2,3,1]

]



c.execute(drop_table_Product)
c.execute(drop_table_Customer)
c.execute(drop_table_Purchase)


c.execute(create_table_Product)
c.execute(create_table_Customer)
c.execute(create_table_Purchase)

for element in values_Product:
    c.execute('INSERT INTO Product VALUES (?,?,?)',element)

for element in values_Customer:
    c.execute('INSERT INTO Customer VALUES (?,?,?)',element)

for element in values_Purchase:
    c.execute('INSERT INTO Purchase VALUES (?,?,?,?,?)',element)





# выполнение execute
conn.commit()

# 1 запрос
request1 = 'SELECT * FROM Purchase WHERE count > 3'

print('Выбрать те покупки, количество товара в которых больше 3х')
for element in c.execute(request1):
    print(element)
print()


# 2 запрос
request2 = 'SELECT * FROM Purchase WHERE strftime("%m", `datetime`)= "01" '
print('Выбрать те покупки, которые совершены в январе')
for element in c.execute(request2):
    print(element)
print()


# 3 запрос
request3 = 'SELECT * FROM Customer WHERE adress LIKE "г.Екатеринбург%" '
print('Выбрать тех покупателей у которых город Екатеринбург')
for element in c.execute(request3):
    print(element)
print()


# 4 запрос
request4 = 'SELECT * FROM Purchase WHERE strftime("%H", `datetime`) >= "22" '
print('Выбрать те покупки которые совершены после 22 часов вечера (и до 00 ночи)')
for element in c.execute(request4):
    print(element)
print()


# 5 запрос
request5 = 'SELECT * FROM Product WHERE price > 35000'
print('Выбрать те товары которые стоят меньше 35 тысяч')
for element in c.execute(request5):
    print(element)
print()


# 6 запрос
request6 = 'SELECT * FROM Product WHERE name LIKE "iphone%" AND price < 35000'
print('Выбрать из товаров iphone которые стоят меньше 35 тысяч')
for element in c.execute(request6):
    print(element)
print()


# 7 запрос

# 8 запрос
request8 = 'SELECT MIN(price) AS MinPrice FROM Product'
print('Найти минимальную цену товара')
for element in c.execute(request8):
    print(element)
print()


# 9 запрос
#request9 = 'SELECT * FROM Product WHERE SELECT MIN(price) FROM Product = price'
#print('Найти товары с минимальной ценой')
#for element in c.execute(request9):
#    print(element)
#print()


#for row in c.execute('SELECT * FROM Product'):
#    print(row)
conn.close()