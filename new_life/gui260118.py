import os
from tkinter import Tk, Button, StringVar, OptionMenu, Text, WORD, END
from tkinter.filedialog import askdirectory
from collections import defaultdict

root = Tk()

# словарь с путём до папки, где нам нужно искать файлы (мы её выбираем сами)
dict_settings = {
    'directory': None,
}

# функция, которая достаёт путь для каждогофайла в заданной папке
def get_list_files(folder):
    files = []
    ext = extention_variable.get() # получаем расширения файлов, которые мы ищем (их мы обозначем в переменной extention_variable после написания функций)
    for root, dirnames, filenames in os.walk(folder):
        for filename in filenames:
            if filename[-len(ext):] == ext: # смотрим совпадает ли расширения файла с нужным
                files.append(os.path.join(root, filename)) # если да то добавляем в массив
    return files # функция возвращает массив с путями до каждого с нужным нам расширением файла


def askopen(event): # функция, которая кладёт в словарь путь к папке, в которой нам нужно искать файлы
    dict_settings["directory"] = askdirectory()


def start(event):
    if dict_settings["directory"] is not None: # если в нашем словаре уже лежит что-то
        files = get_list_files(dict_settings["directory"]) # то дастаём пути до файла с помощью нашей функции, написанной ранее get_list_files

        # создаём два словаря (которые ничем не отличается от обычных
        # за исключением того, что по умолчанию всегда вызывается функция, возвращающая значение)
        result_dict = defaultdict(list)
        result2_dict = defaultdict(list)

        for file in files:
            result_dict[os.path.getsize(file)].append(file) # в один из этих словарей кладём размеры файлов

        for size, files in result_dict.items(): # бежим по созданному словарю с размерами файлов
            if len(files) > 1:
                for file in files:
                    result2_dict[hash(open(file, 'rb').read())].append(file)
            else:
                result2_dict[size] = result_dict[size]

        result_list = []
        for key, files in result2_dict.items():
            if len(files) > 1:
                for file in files:
                    result_list.append(file)
                result_list.append('\n')
        result_text.delete('1.0', END)
        result_text.insert('1.0', "\n".join(result_list))


extentions = (".pdf", ".djvu") # расширения файлов, которые мы ищем

extention_variable = StringVar(root)
extention_variable.set(extentions[0])

# обозначаем параметры кнопки
params_btn = {
    'width': 30,
    'height': 5,
    'bg': "white",
    'fg': "black",
}

result_text = Text(root, width=40,
                   font="Verdana 12",
                   wrap=WORD)

extention_menu = OptionMenu(root,
                            extention_variable, *extentions)
# текст, который будет выводиться на каждой из кнопки
btn_select_folder = Button(root,
                           text="Выберите папку", **params_btn)

btn_start = Button(root,
                   text="Искать", **params_btn)

# отображаем кнопки на экран
btn_select_folder.bind("<Button-1>", askopen)
btn_start.bind("<Button-1>", start)

btn_select_folder.pack()
extention_menu.pack()
btn_start.pack()
result_text.pack()

root.mainloop()