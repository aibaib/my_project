
import multiprocessing
import sys
import os

# та же самая программа (калькулятор), только с multiprocessingom

# Функция inputer
def inputer(q_input, fun, n):

    sys.stdin = os.fdopen(fun) # читаем через sys.stdin

    while True :  # бесконечный цикл в котором считываем данные

        A = input().split() # считываем данные

        if A[0] != 'stop' :  # выход из этого бесконечного цикла при введении слова stop (т.е. прекращение программы, когда вводим stop)
            q_input.put(A)  # считанные данные кладём в очередь
        else:
            for i in range(n):  # если ввели слово stop
                q_input.put(None)  # то в очередь кладётся None столько раз, сколько есть потоков
            break  # выходим из бесконечного цикла


# функция outputer
def outputer(q_output, fun, n, flag):

    sys.stdin = os.fdopen(fun) # читаем через sys.stdin

    while True :  # бесконечный цикл
        out = q_output.get()  # достаём из очереди
        if out is None:  # если в очереди None
            flag += 1  # делаем счётчик
            if flag == n:  # выходим, когда все wokerы выключатся
                break  # завершаем
        else:
            print(out)  # выводим то что лежит в очереди


# функция worker
def worker(q_input, q_output):
    while True :  # снова бесконечный цикл
        B = q_input.get() # достаём элемент из очереди

        if B is None:  # если из очереди получили None
            q_output.put(None) # то кладём None
            break # выход из бесконечного цикла
        else:  # если всё хорошо, то делаем арифметические операции
            znak = B[1]
            chislo1 = int(B[0])
            chislo2 = int(B[2])

            a=abs(chislo1)**abs(chislo2)**100





            q_output.put(a)  # кладём результат в очередь


if __name__ == '__main__':
    q_output = multiprocessing.Queue()  # создаём две очереди
    q_input = multiprocessing.Queue()

    fun = sys.stdin.fileno()

    flag = 0 # счётчик

    n = 8  # кол-во процессов
    tk = [None] * n # создаём массив workerов


    # начинаем работу с процессами
    t1 = multiprocessing.Process(target=inputer, args=(q_input, fun, n))
    for i in range(n):
        tk[i] = multiprocessing.Process(target=worker, args=(q_input, q_output))
    t3 = multiprocessing.Process(target=outputer, args=(q_output, fun, n, flag))

    # запускаем
    t1.start()
    for i in range(n):
        tk[i].start()
    t3.start()

    # ждем прекращения работы дочерних потоков
    t1.join()
    for i in range(n):
        tk[i].join()
    t3.join()