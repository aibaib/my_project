import queue
from threading import Thread


# Функция inputer
def inputer():

    while True :  # бесконечный цикл в котором считываем данные

        A = input().split() # считываем данные


        if A[0] != 'exit' :  # выход из этого бесконечного цикла при введении слова exit (т.е. прекращение программы, когда вводим stop)
            q_input.put(A)  # считанные данные кладём в очередь
        else:
            for i in range(n):  # если ввели слово stop
                q_input.put(None)  # то в очередь кладётся None столько раз, сколько есть потоков
            break  # выходим из бесконечного цикла


# функция outputer
def outputer():

    kol = 0 # счётчик

    while True :  # бесконечный цикл
        out = q_output.get()  # достаём из очереди
        if out is None:  # если в очереди None
            kol += 1  # делаем счётчик
            if kol == n:  # выходим, когда все wokerы выключатся
                break  # завершаем
        else:
            print(out)  # выводим то что лежит в очереди


# функция worker
def worker():
    while True :  # снова бесконечный цикл
        B = q_input.get() # достаём элемент из очереди

        if B is None:  # если из очереди получили None
            q_output.put(None) # то кладём None
            break # выход из бесконечного цикла
        else:  # если всё хорошо, то делаем арифметические операции
            znak = B[1]
            chislo1 = int(B[0])
            chislo2 = int(B[2])

            # fixme обработку лучше убирать в отдельную функцию
            if znak == '+':
                a = chislo1 + chislo2
            elif znak == '-':
                a = chislo1 - chislo2
            elif znak == '*':
                a = chislo1 * chislo2
            else:
                a = chislo1 / chislo2

            q_output.put(a)  # кладём результат в очередь


if __name__ == '__main__':
    q_output = queue.Queue()  # создаём две очереди
    q_input = queue.Queue()

    n = 3  # кол-во потоков
    tk = [None] * n # создаём массив workerов


    # создаём объекты
    t1 = Thread(target=inputer)
    for i in range(n):
        tk[i] = Thread(target=worker)
    t3 = Thread(target=outputer)

    # запускаем
    t1.start()
    for i in range(n):
        tk[i].start()
    t3.start()

    # ждем прекращения работы дочерних потоков
    t1.join()
    for i in range(n):
        tk[i].join()
    t3.join()
