import queue
from threading import Thread
from PIL import Image
import pathlib



# функция обработки изображений (с прошлого года)
def obrabotka_of_images (Name, dvig, pyt):

    if dvig == 'Black_White':  # копированный код с прошлого года, который преобразуе в чёрно-белое изображение

        img = Image.open(str(pyt)).convert('RGB')
        h, lenn = img.size
        for i in range(h):
            for j in range(lenn):
                RGB = img.getpixel((i, j))
                r = RGB[0]
                g = RGB[1]
                b = RGB[2]
                img.putpixel((i, j), ((r + g + b) // 3, (r + g + b) // 3, (r + g + b) // 3))

    elif dvig == 'Iversya':  # копированный код: Инверсия

        img = Image.open(str(pyt)).convert('RGB')
        h, l = img.size
        for i in range(h):
            for j in range(l):
                RGB = img.getpixel((i, j))
                r = RGB[0]
                g = RGB[1]
                b = RGB[2]
                img.putpixel((i, j), (b, g, r))

    name_of_file = pyt[pyt.rfind("s")+2 : pyt.rfind('.')] # имя изображения ( делаем слайс от пути)

    rezalt = [Name, img, dvig, name_of_file]
    return rezalt  # функция возвращает список из 4 элементов: имя которому принадлежит фотография, саму фотографию
                    # также действие, которое мы сделали с этой фотографией и её название




# Функция inputer
def inputer():

    while True :  # бесконечный цикл в котором считываем данные

        A = filic.readline().split() # читаю содиржимое файлика

        if len(A) > 2:  # выход из этого бесконечного цикла при введении слова stop или exit
            q_input.put(A)  # считанные данные кладём в очередь

        else:

            for i in range(n):  # если ввели слово exit или stop
                q_input.put([None])  # то в очередь кладётся None столько раз, сколько есть потоков
            break  # выходим из бесконечного цикла




# функция worker
def worker():
    while True :  # снова бесконечный цикл

        B = q_input.get() # считываем содержимое нашей очереди

        if len(B) == 1: # если в очереди лежит только 1 None, то нужно заканчивать....
            q_output.put([None])  # кладём в очередь снова только 1 None
            break

        else: # если всё хорошо, то делаем обработку изображений

            Name, dvig, pyt = B # если у нас доcталось 3 переменныех, то передаём их функции, которая их меняет
            rezalt = obrabotka_of_images(Name, dvig, pyt)
            q_output.put(rezalt)  # в очередь кладём список из 4 элементов
                                    # (каких именно можно посмотреть в функции obrabotka_of_images)



# функция outputer
def outputer():

    kol = 0 # счётчик

    while True :  # бесконечный цикл

        V = q_output.get() # достаём из очереди то что тамм лежит


        if len(V) == 1:  # если в очереди 1 None, то завершаем
            kol += 1  # делаем счётчик

            if kol == n:  # выходим, когда все wokerы выключатся
                break  # завершаем

        else: # иначе сохраняем изменённые изображения

            people, picture, dvig, name_of_file = V # достаём из очереди 4 значения, которые положили в workerе

            pathlib.Path('./{}'.format(str(people))).mkdir(parents=True, exist_ok=True) # создаём папку с именем человека, которому
                                     # принадлежит изображение (с помощью Гугла)
            picture.save('{}.jpg'.format(str(people) + '/' + str(name_of_file) + '_' + str(dvig)))
                    # сохраняем в эту папку изменённое изображение, под именем: прежнее название и действие, которое мы с ним сделали



if __name__ == '__main__':

    q_output = queue.Queue()  # создаём две очереди
    q_input = queue.Queue()

    filic = open('input.txt', 'r') # открываем файлик, в котором записаны данные

    n = 3  # кол-во потоков

    tk = [None] * n # создаём массив workerов


    # создаём объекты
    t1 = Thread(target=inputer)
    for i in range(n):
        tk[i] = Thread(target=worker)
    t3 = Thread(target=outputer)

    # запускаем
    t1.start()
    for i in range(n):
        tk[i].start()
    t3.start()

    # ждем прекращения работы дочерних потоков
    t1.join()
    for i in range(n):
        tk[i].join()
    t3.join()

