def IsAscending (a):
    i=0
    while i!=len(a)-1 and a[i]<a[i+1]:
        i+=1
    return i


a=list(map(int,input().split()))
if IsAscending(a)==len(a)-1:
    print("YES")
else:
    print('NO')
