def SelectionSort(a):
    for j in range(len(a)-1):
        max_i=j
        for i in range(j,len(a)):
            if a[i]>a[max_i]:
                max_i=i
        a[j],a[max_i]=a[max_i],a[j]
a=list(map(int,input().split()))
SelectionSort(a)
print(' '.join(map(str,a)))