def BubbleSort(a):
    for j in range(len(a)):
        for i in range(len(a)-j-1):
            if a[i]<a[i+1]:
                a[i],a[i+1]=a[i+1],a[i]

a=list(map(int,input().split()))
BubbleSort(a)
print(' '.join(map(str,a)))