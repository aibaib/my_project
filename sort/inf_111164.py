def my_max(mas) :
    index,m =0,mas[0]
    for i in range(len(mas)):
        if m<mas[i]:
            index,m=i,mas[i]
    return index


def merge_sort(mas1,mas2):
    mas3=[]
    i=0
    j=0
    while i<=len(mas1)-1 and j<=len(mas2)-1:
        if mas1[i]<mas2[j]:
            mas3.append(mas1[i])
            i+=1
        else:
            mas3.append(mas2[j])
            j+=1
    mas3+=mas1[i:]+mas2[j:]
    return mas3
def s_m(mas):
    mas1,mas2=mas[:len(mas)//2],mas[len(mas)//2:]
    mas1=s_m(mas1) if len(mas1)>1 else mas1
    mas2=s_m(mas2) if len(mas2)>1 else mas2
    mas3=merge_sort(mas1,mas2)
    return mas3


a=int(input())
b=list(map(int,input().split()))
m=my_max(b)
n=[]
z=[]
for i in range(m+1,a):
    n.append(b[i])
max=0
for i in range(len(n)-1):
    if n[i]%5==0 and n[i]%10!=0 and n[i+1]<n[i]:
        if n[i]>max:
            max=n[i]

b=s_m(b)
#b.sort()
p=[]
for i in range(a):
    if b[i]==max:
        p.append(a-i)
if len(p)==0:
    print(0)
else:
    print(min(p))
