def CountSort(a):
    c = [0] * 130
    n=[]
    for i in range(len(a)):
        c[a[i]]+= 1
    for i in range(0,130):
        if c[i]!=0:
            for j in range(c[i]):
                n.append(i)
    return n
a =list(map(int,input().split()))
n=CountSort(a)
print(' '.join(map(str,n)))