def merge(m1,m2):
    m3=[]
    i=0
    j=0
    while i<=len(m1)-1 and j<=len(m2)-1:
        if m1[i]<m2[j]:
            m3.append(m1[i])
            i+=1
        else:
            m3.append(m2[j])
            j+=1
    m3+=m1[i:]+m2[j:]
    return m3

m1=list(map(int,input().split()))
m2=list(map(int,input().split()))
m3=merge(m1,m2)
print(' '.join(map(str,m3)))