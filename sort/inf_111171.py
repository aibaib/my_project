def Intersection(a,b):
    n=[]
    i=0
    j=0
    while i!=len(a) and j!=len(b):
        if a[i]==b[j]:
            n.append(a[i])
            i+=1
            j+=1
        elif a[i]<b[j]:
            i+=1
        elif a[i]>b[j]:
            j+=1
    return n
a=list(map(int,input().split()))
b=list(map(int,input().split()))
c=Intersection(a,b)
print(' '.join(map(str,c)))
