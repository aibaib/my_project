class Steck():
    def __init__(self):
        self.a=[]

    def push(self,val):
        self.a.append(val)
        print('ok')

    def pop(self):
        if len(self.a)!=0:
            print(self.a.pop())
        else:
            print('error')

    def back(self):
        if len(self.a)!=0:
            print(self.a[-1])
        else:
            print('error')

    def size(self):
        print(len(self.a))

    def clear(self):
        self.a=[]
        print('ok')

    def exit(self):
        print('bye')
        exit()

steck=Steck()
while True:
    k=input().split()
    getattr(steck,k[0])(*k[1:])
