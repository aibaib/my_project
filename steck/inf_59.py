class Turn():
    def __init__(self):
        self.a=[]

    def push(self,val):
        self.a.append(val)
        print 'ok'

    def pop(self):
        if len(self.a)!=0:
            print self.a.pop(0)
        else:
            print 'error'

    def front(self):
        if len(self.a)!=0:
            print self.a[0]
        else:
            print 'error'

    def size(self):
        print len(self.a)

    def clear(self):
        self.a=[]
        print 'ok'

    def exit(self):
        print 'bye'
        exit()

turn=Turn()
while True:
    k=raw_input().split()
    getattr(turn,k[0])(*k[1:])