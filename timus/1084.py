import math
a,r=list(map(int,input().split()))
p=3.1415926535
if r<=a/2:
    #print(p*(r**2))
    summ=p*(r**2)
    print("%.3f" % summ)
elif r>=a/2*(2**0.5):
    summ=a**2
    print("%.3f" % summ)
else:
    h=r-a/2
    e=(2*r*h-h*h)**0.5
    k=math.acos((r-h)/r)
    S=r*r*k-(r-h)*e
    g=p*(r**2)
    summ=g-4*S

    print("%.3f" % summ)