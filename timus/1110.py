n,m,y=map(int,input().split())
s=[]
for i in range(m):
    if (i**n)%m==y:
        s.append(i)
if len(s)==0:
    print(-1)
else:
    print(' '.join(map(str,s)))

