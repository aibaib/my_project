a,c=map(int,input().split())
b=[0 for i in range(a)]
for i in range(c) :
    j = int(input())
    b[j-1] += 1

def F(s) :
    while len(s) < 5 :
        s += '0'
    return s

for i in range(a):
    print('{0:.2f}'.format((b[i]/c)*100),end='')
    print('%')