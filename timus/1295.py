n=int(input())
if n==1:
    print(1)
elif n%4==0:
    print(0)
elif n%4==3 or n==5 or n==25:
    print(2)
elif n%4==1 and n%5==0:
    print(2)
else:
    print(1)