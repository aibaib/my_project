n=int(input())
a=[]
for i in range(n**2):
    a.append(int(input()))

a.sort()
x=0
h=(n-1)*2+1
y=len(a)-h
#print(a)
#print(y)
s=[]
for j in range(n):
    s.append([0]*n)

for i in range(n):
    for j in range(n):
        if j==n//2 and i==n//2:
            s[i][j]=a[n**2-1]
        elif j==n//2 or i==n//2:
            s[i][j]=a[y]
            y+=1
        elif (j==0 and i==0) or (j==0 and i==n-1) or (j==n-1 and i==0) or (j==n-1 and i==n-1):
            s[i][j]=a[x]
            x+=1
        else:
            s[i][j]=a[x]
            x+=1

for i in range(n):
    print(' '.join(map(str,s[i])))