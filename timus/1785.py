a=int(input())
if a>=1 and a<=4:
    print('few')
elif a>=5 and a<=9:
    print('several')
elif a>=10 and a<=19:
    print('pack ')
elif a>=20 and a<=49:
    print('lots')
elif a>=50 and a<=99:
    print('horde')
elif a>=100 and a<=249:
    print('throng')
elif a>=250 and a<=499:
    print('swarm')
elif a>=500 and a<=999:
    print('zounds')
else:
    print('legion')