p,n,V,T='','','',''
for i in range(3):
    a=input()
    if a[0] == 'p' or a[0] == 'P':
        p = int(a[4:])
    elif a[0] == 'n' or a[0] == 'N':
        n = int(a[4:])
    elif a[0] == 'V' or a[0] == 'v':
        V = int(a[4:])
    elif a[0] == 'T' or a[0] == 't':
        T = int(a[4:])


r=8.314
error='error'
undefined='undefined'



if p=='':
    p=(n*r*T)/V
    print('p = {}'.format(round(p,6)))

elif n=='':
    n=(p*V)/(r*T)
    print('n = {}'.format(round(n,6)))

elif p==0 and n==0:
    print(undefined)

elif (p==0 and n!=0) or (p!=0 and n==0):
    print(error)

elif T=='':
    if n==0:
        print(error)
    elif p==0:
        print(undefined)
    else:
        T=(p*V)/(n*r)

        if T<=0:
            print(error)
        else:
            print('T = {}'.format(round(T,6)))

elif V=='':
    if p==0:
        print(error)
    elif n==0:
        print(undefined)
    else:
        V=(n*r*T)/p

        if V<=0:
            print(error)
        else:
            print('V = {}'.format(round(V,6)))
