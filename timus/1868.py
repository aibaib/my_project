s1=[]
s2=[]
s3=[]
for i in range(12):
    a=input()
    if i<4:
        s1.append(a)
    elif i>=4 and i<8:
        s2.append(a)
    else:
        s3.append(a)


n=int(input())

summ=0
maxx=0
k=1

for j in range(n):
    b=int(input())
    for i in range(b):
        c=input().split()
        if c[2]=='gold':
            if c[0] in s1:
                summ+=1
        elif c[2]=='silver':
            if c[0] in s2:
                summ+=1
        else:
            if c[0] in s3:
                summ+=1

    if summ==maxx:
        k+=1
    elif summ>maxx:
        maxx=summ
        k=1

    summ=0
#print(maxx)
if maxx==0:
    print(5*n)
else:
    print(k*5)