"""
n,k=map(int,input().split())
s1=0 #ожидаемая сумма
s2=0 #введённая сумма
for i in range(n):
    a,b=map(int,input().split())
    s1+=a
    s2+=b
q=(s1+k)-((n+1)*2)
e=q-s2
#print(e)
if e<0 or e>100:
    print('Big Bang!')
else:
    print(e)

"""
n,k=list(map(int,input().split()))
summ1=0
summ2=0
for i in range(n):
    b,g=list(map(int,input().split()))
    summ1+=b
    summ2+=g
e=summ1+k-(n+1)*2
q=e-summ2
if q<0:
    print('Big Bang!')
else:
    print(q)

