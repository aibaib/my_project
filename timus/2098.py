b=int(input())
c,d=list(map(int,input().split()))
k,p=list(map(int,input().split()))

summ1=b
summ2=0

for i in range(1,k+1):
    summ1+=c+d*i
    summ2+=(c+d*i)*(100+p)

summ1=100*summ1

if summ1<summ2:
    print('Cash')
    print((summ2-summ1)/100)
else:
    print('Insurance')
    print((summ1-summ2)/100)
