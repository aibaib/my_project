from  random import randint as r
from random import random
from matplotlib import pyplot as plt
from matplotlib import animation



#функция столкновения и изменения цвета
def get_color(i):
    for i in range(100):
        for j in range(100):
            if (x[i]-x[j])**2+(y[i]-y[j])**2<=0.04**2 and (color[i]=='r' or color[j]=='r'):
                color[i]=color[j]='r' #изменяет цвет


    return color #возвращает массив цветов


#функция координат после каждой 1/200 секунды
def koordinat(i):

    for i in range(100):

        x[i]=x[i]+(1/200)*Vx[i]
        y[i]=y[i]+(1/200)*Vy[i]

        if x[i]>2:
            x[i]=x[i]-2
        elif x[i]<0:
            x[i]=x[i]+2
        elif y[i]>2:
            y[i]=y[i]-2
        elif y[i]<0:
            y[i]=y[i]+2

    return x,y#возвращает массив x и y после каждой итеррации

#функция которая возвращает массив x после каждой итеррации
def get_x(i):
    x1,y1=koordinat(i)
    x=x1
    return x#возвращает массив x


#функция которая возвращает массив y после каждой итеррации
def get_y(i):
    x1, y1 = koordinat(i)
    y=y1
    return y#возвращает массив y


#основной код
x=[]
y=[]
Vx=[]
Vy=[]
color=[]

for i in range(100):#создаём рандомом массивы
    k1=random()*2-1
    x.append(k1)

    k2=random()*2-1
    y.append(k2)

    k3=random()*2-1
    Vx.append(k3)

    k4=random()*2-1
    Vy.append(k4)

    color.append('b')


m=r(0,100)#выбираем рандобно точку которая больна
color[m]='r'#делаем её больной


def init():
    return []

summ=0
Z=[]
W=[]
def animate(i):
    summ=0
    x=get_x(i)
    y=get_y(i)
    color=get_color(i)
    #print(color)
    blux,redx,bluy,redy=[[],[],[],[]]

    for i in range(100):
        if color[i]=='b':
            blux.append(x[i])
            bluy.append(y[i])
        else:
            redx.append(x[i])
            redy.append(y[i])
            summ+=1
    Z.append(summ)


    return ax.plot(blux, bluy, 'bo', redx, redy, 'ro', antialiased=False, ms=3)

fig = plt.figure()
ax = plt.axes(xlim=(0, 2), ylim=(0, 2))

anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=200, interval=20, blit=True)


#anim.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])

plt.show()


graph = plt.figure() #строим график
ax = graph.add_subplot(111)
ax.plot( Z, 'r')
plt.xlabel('number of iterration')  # подписываем оси
plt.ylabel('number of sick tochek')
plt.show()